# Overview

## Installation

```bash
npm install math.gl
```

## Usage

```js
import {Vector2} from 'math.gl';
const vector = new Vector2(1, 2);
```
